# Create your views here.
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render_to_response
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.template import RequestContext
from purchase.models import Sale
from purchase.forms import SalePaymentForm

import datetime
import uuid
import json
import stripe




def login_user(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')

		user = authenticate(username=username, password=password)

		#if user is not null
		if user is not None:
			if user.is_active:
				login(request, user)
				redirect('purchase.views.purchase')
			else:
				state = "User account disabled"
		else:
			state = "User not defined"
	else:
		state = "User credentials invalid"


def login_successful(request):
	return render(request, 'home.html')

def purchase(request):
	if request.method == "POST":
		form = SalePaymentForm(request.POST)
 
		if form.is_valid(): # charges the card
        	return HttpResponse("Success! We've charged your card!")
    else:
        form = SalePaymentForm()
 
    return render_to_response("purchase.html",
                        RequestContext( request, {'form': form} ) )
	





