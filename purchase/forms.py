from django import forms
from datetime import date, datetime

from purchase.models import Sale

# Define widgets and fields for payment form
class CreditCardField(forms.IntegerField):
	def clean(self, value):
		#Credit card number must be between 13 and 16 digits
		if len(value) < 13 or len(value) > 16:
			raise forms.ValidationError("Please enter in a valid credit card number")
		return super(CreditCardField, self).clean(value)

class SalePaymentForm(forms.Form):

	number = CreditCardField(required=True, label="Card Number")
	exp_month = forms.IntegerField(required=True)
	exp_year = forms.IntegerField(required=True)
	cvc = forms.IntegerField(required=True, label="CVC", min_value=4, max_value=4)

	def clean_cvc(self, value):

		if len(value)!=4:
			raise forms.ValidationError("CVC must be 4 digits")
		return super(forms.Form, self).clean(value)

	def clean(self):

		## The user is charged 10 usd

		cleaned = super(SalePaymentForm, self).clean()
		if cleaned:
			number = self.cleaned_data['number']
			exp_month = self.cleaned_data['exp_month']
			exp_year = self.cleaned_data['exp_year']
			cvc = self.cleaned_data['cvc']

			# Create new sale instance
			sale = Sale()

			# Example charge 
			success, instance = sale.charge(1000, number, expiration, cvc)

			if not success:
				raise forms.ValidationError("Error %s" % instance.message)
			else:
				instance.save()
				# Send confirmation email
		return cleaned
		