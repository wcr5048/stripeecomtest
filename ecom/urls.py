from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

#superuser = root 
#password = password


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ecom.views.home', name='home'),
    # url(r'^ecom/', include('ecom.foo.urls')),
    url(r'^login$', 'purchase.views.login_user', name='login'),
    url(r'^login_successful$', 'purchase.views.login_successful'),
    url(r'^purchase/$', 'purchase.views.purchase', name="purchase"),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
